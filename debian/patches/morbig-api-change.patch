Author: Nicolas Jeannerod
Description: Follow morbig API change

diff --git a/src/AST.ml b/src/AST.ml
index 44f9e3b..63f3467 100644
--- a/src/AST.ml
+++ b/src/AST.ml
@@ -31,11 +31,11 @@ and character_range = char list
 
 and attribute =
   | NoAttribute
-  | ParameterLength of word
-  | UseDefaultValues of word
-  | AssignDefaultValues of word
-  | IndicateErrorifNullorUnset of word
-  | UseAlternativeValue of word
+  | ParameterLength
+  | UseDefaultValues of word * bool
+  | AssignDefaultValues of word * bool
+  | IndicateErrorifNullorUnset of word * bool
+  | UseAlternativeValue of word * bool
   | RemoveSmallestSuffixPattern of word
   | RemoveLargestSuffixPattern of word
   | RemoveSmallestPrefixPattern of word
@@ -49,7 +49,12 @@ and word_component =
   | WSubshell of program
   | WGlobAll
   | WGlobAny
-  | WBracketExpression of (Morbig.CST.bracket_expression [@equal (=)] [@opaque])
+  | WBracketExpression of (
+      Morbig.CST.bracket_expression
+      [@equal (=)] [@opaque]
+      [@to_yojson Morbig.CSTSerializers.bracket_expression_to_yojson]
+      [@of_yojson Morbig.CSTSerializers.bracket_expression_of_yojson]
+    )
 
 and word = word_component list
 and word' = word Location.located
@@ -191,7 +196,7 @@ and command =
   | Simple of assignment' list * word' list
 
   (* Lists *)
-  | Async of command
+  | Async of command'
   | Seq of command' * command'
   | And of command' * command'
   | Or of command' * command'
@@ -202,8 +207,8 @@ and command =
 
   (* Compound Command's *)
   | Subshell of command'
-  | For of name * word list option * command'
-  | Case of word * case_item' list
+  | For of name * word' list option * command'
+  | Case of word' * case_item' list
   | If of command' * command' * command' option
   | While of command' * command'
   | Until of command' * command'
@@ -212,7 +217,7 @@ and command =
   | Function of name * command'
 
   (* Redirection *)
-  | Redirection of command' * descr * kind * word
+  | Redirection of command' * descr * kind * word'
   | HereDocument of command' * descr * word'
 
 and command' = command Location.located
diff --git a/src/CST_to_AST.ml b/src/CST_to_AST.ml
index 8890c9a..0f84437 100644
--- a/src/CST_to_AST.ml
+++ b/src/CST_to_AST.ml
@@ -62,7 +62,7 @@ and complete_commands'__to__command'_list (complete_commands' : complete_command
 
 and complete_command__to__command = function
   | CompleteCommand_CList_SeparatorOp (clist', sepop') ->
-     clist'__to__command clist'
+     clist'__to__command' clist'
      |> separator_op'__to__command sepop'
   | CompleteCommand_CList clist' ->
      clist'__to__command clist'
@@ -199,7 +199,7 @@ and compound_list__to__command : compound_list -> AST.command = function
   | CompoundList_LineBreak_Term (_, term') ->
      term'__to__command term'
   | CompoundList_LineBreak_Term_Separator (_, term', sep') ->
-     term'__to__command term'
+     term'__to__command' term'
      |> separator'__to__command sep'
 
 and compound_list'__to__command (compound_list' : compound_list') : AST.command =
@@ -245,7 +245,7 @@ and for_clause__to__command : for_clause -> AST.command = function
   | ForClause_For_Name_LineBreak_In_WordList_SequentialSep_DoGroup (name', _, wordlist', _, do_group') ->
      AST.For (
          name'__to__name name' ,
-         Some (wordlist'__to__word_list wordlist') ,
+         Some (wordlist'__to__word'_list wordlist') ,
          do_group'__to__command' do_group'
        )
 
@@ -254,32 +254,32 @@ and for_clause'__to__command (for_clause' : for_clause') : AST.command =
 
 (* CST.wordlist -> AST.word list *)
 
-and wordlist__to__word_list : wordlist -> AST.word list = function (*FIXME*)
+and wordlist__to__word'_list : wordlist -> AST.word' list = function
   | WordList_WordList_Word (wordlist', word') ->
-     (wordlist'__to__word_list wordlist')
-     @ [word'__to__word word']
+     (wordlist'__to__word'_list wordlist')
+     @ [word'__to__word' word']
   | WordList_Word word' ->
-     [word'__to__word word']
+     [word'__to__word' word']
 
-and wordlist'__to__word_list (wordlist' : wordlist') : AST.word list =
-  erase_location wordlist__to__word_list wordlist'
+and wordlist'__to__word'_list (wordlist' : wordlist') : AST.word' list =
+  erase_location wordlist__to__word'_list wordlist'
 
 (* CST.case_clause -> AST.command *)
 
 and case_clause__to__command : case_clause -> AST.command = function
   | CaseClause_Case_Word_LineBreak_In_LineBreak_CaseList_Esac (word', _, _, case_list') ->
      AST.Case (
-         word'__to__word word' ,
+         word'__to__word' word' ,
          case_list'__to__case_item'_list case_list'
        )
   | CaseClause_Case_Word_LineBreak_In_LineBreak_CaseListNS_Esac (word', _, _, case_list_ns') ->
      AST.Case (
-         word'__to__word word' ,
+         word'__to__word' word' ,
          case_list_ns'__to__case_item'_list case_list_ns'
        )
   | CaseClause_Case_Word_LineBreak_In_LineBreak_Esac (word', _, _) ->
      AST.Case (
-         word'__to__word word' ,
+         word'__to__word' word' ,
          []
        )
 
@@ -625,20 +625,20 @@ and redirect_list'__to__command (redirect_list' : redirect_list') (command' : AS
 and io_redirect__to__command (io_redirect : io_redirect) (command' : AST.command') : AST.command =
   match io_redirect with
   | IoRedirect_IoFile io_file' ->
-     let kind, word = io_file'__to__kind_word io_file' in
+     let kind, word' = io_file'__to__kind_word' io_file' in
      AST.Redirection (
          command' ,
          (AST.default_redirection_descriptor kind) ,
          kind ,
-         word
+         word'
        )
   | IoRedirect_IoNumber_IoFile (io_number, io_file') ->
-     let kind, word = io_file'__to__kind_word io_file' in
+     let kind, word' = io_file'__to__kind_word' io_file' in
      AST.Redirection (
          command' ,
          (io_number__to__int io_number) ,
          kind ,
-         word
+         word'
        )
   | IoRedirect_IoHere io_here' ->
      let _strip, word' = io_here'__to__strip_word' io_here' in
@@ -663,7 +663,7 @@ and io_redirect'__to__command' (io_redirect' : io_redirect') (command' : AST.com
 
 (* CST.io_file -> AST.redirection_kind * AST.word *)
 
-and io_file__to__kind_word io_file =
+and io_file__to__kind_word' io_file =
   let kind, filename' =
     match io_file with
     | IoFile_Less_FileName filename' -> AST.Input, filename'
@@ -674,19 +674,19 @@ and io_file__to__kind_word io_file =
     | IoFile_LessGreat_FileName filename' -> AST.InputOutput, filename'
     | IoFile_Clobber_FileName filename' -> AST.OutputClobber, filename'
   in
-  ( kind , filename'__to__word filename' )
+  ( kind , filename'__to__word' filename' )
 
-and io_file'__to__kind_word (io_file' : io_file') : AST.kind * AST.word =
-  erase_location io_file__to__kind_word io_file'
+and io_file'__to__kind_word' (io_file' : io_file') : AST.kind * AST.word' =
+  erase_location io_file__to__kind_word' io_file'
 
 (* CST.filename -> AST.word *)
 
-and filename__to__word : filename -> AST.word = function
+and filename__to__word' : filename -> AST.word' = function
   | Filename_Word word' ->
-     word'__to__word word'
+     word'__to__word' word'
 
-and filename'__to__word (filename' : filename') : AST.word =
-  erase_location filename__to__word filename'
+and filename'__to__word' (filename' : filename') : AST.word' =
+  erase_location filename__to__word' filename'
 
 (* CST.io_here -> bool * AST.word *)
 
@@ -701,37 +701,35 @@ and io_here'__to__strip_word' (io_here' : io_here') : bool * AST.word' =
 
 (* CST.separator_op -> AST.command -> AST.command *)
 
-and separator_op__to__command (sep_op : separator_op) (command : AST.command) : AST.command =
+and separator_op__to__command (sep_op : separator_op) (command' : AST.command') : AST.command =
   match sep_op with
-  | SeparatorOp_Uppersand -> AST.Async command
-  | SeparatorOp_Semicolon -> command
+  | SeparatorOp_Uppersand -> AST.Async command'
+  | SeparatorOp_Semicolon -> command'.value
 
-and separator_op'__to__command (sep_op' : separator_op') (command : AST.command) : AST.command =
-  erase_location separator_op__to__command sep_op' command
+and separator_op'__to__command (sep_op' : separator_op') (command' : AST.command') : AST.command =
+  erase_location separator_op__to__command sep_op' command'
 
 and separator_op'__to__command' (sep_op' : separator_op') (command' : AST.command') : AST.command' =
   (* We do not want to convert the separator's location here but
      rather use the command's location! *)
-  { value = separator_op__to__command sep_op'.value command'.value ;
-    position = command'.position }
+  Location.copy_location command' (separator_op'__to__command sep_op' command')
 
 (* CST.separator -> AST.command -> AST.command *)
 
-and separator__to__command (sep : separator) (command : AST.command) : AST.command =
+and separator__to__command (sep : separator) (command' : AST.command') : AST.command =
   match sep with
   | Separator_SeparatorOp_LineBreak (sep_op', _) ->
-     separator_op'__to__command sep_op' command
+     separator_op'__to__command sep_op' command'
   | Separator_NewLineList _ ->
-     command
+     command'.value
 
-and separator'__to__command (sep' : separator') (command : AST.command) : AST.command =
-  erase_location separator__to__command sep' command
+and separator'__to__command (sep' : separator') (command' : AST.command') : AST.command =
+  erase_location separator__to__command sep' command'
 
 and separator'__to__command' (sep' : separator') (command' : AST.command') : AST.command' =
   (* We do not want to convert the separator's location here but
      rather use the command's location! *)
-  { value = separator__to__command sep'.value command'.value ;
-    position = command'.position }
+  Location.copy_location command' (separator'__to__command sep' command')
 
 (* *)
 
@@ -817,16 +815,16 @@ and word_component_double_quoted__to__word = function
 and variable_attribute__to__attribute = function
   | NoAttribute ->
     AST.NoAttribute
-  | ParameterLength word ->
-    AST.ParameterLength (word__to__word word)
-  | UseDefaultValues (_, word) ->
-    AST.UseDefaultValues (word__to__word word)
-  | AssignDefaultValues (_, word) ->
-    AST.AssignDefaultValues (word__to__word word)
-  | IndicateErrorifNullorUnset (_, word) ->
-    AST.IndicateErrorifNullorUnset (word__to__word word)
-  | UseAlternativeValue (_, word) ->
-    AST.UseAlternativeValue (word__to__word word)
+  | ParameterLength ->
+    AST.ParameterLength
+  | UseDefaultValues (p, word) ->
+    AST.UseDefaultValues (word__to__word word, p.[0] = ':')
+  | AssignDefaultValues (p, word) ->
+    AST.AssignDefaultValues (word__to__word word, p.[0] = ':')
+  | IndicateErrorifNullorUnset (p, word) ->
+    AST.IndicateErrorifNullorUnset (word__to__word word, p.[0] = ':')
+  | UseAlternativeValue (p, word) ->
+    AST.UseAlternativeValue (word__to__word word, p.[0] = ':')
   | RemoveSmallestSuffixPattern word ->
     AST.RemoveSmallestSuffixPattern (word__to__word word)
   | RemoveLargestSuffixPattern word ->
diff --git a/src/location.ml b/src/location.ml
index 474ae35..65dcaec 100644
--- a/src/location.ml
+++ b/src/location.ml
@@ -36,16 +36,20 @@ type 'a located = 'a Morbig.CST.located =
     position : position }
 [@@deriving eq, show {with_path=false}, yojson]
 
-class virtual ['a] located_iter      = ['a] Morbig.CST.located_iter
-class virtual ['a] located_map       = ['a] Morbig.CST.located_map
-class virtual ['a] located_reduce    = ['a] Morbig.CST.located_reduce
-class virtual ['a] located_mapreduce = ['a] Morbig.CST.located_mapreduce
-class virtual ['a] located_iter2     = ['a] Morbig.CST.located_iter2
-class virtual ['a] located_map2      = ['a] Morbig.CST.located_map2
-class virtual ['a] located_reduce2   = ['a] Morbig.CST.located_reduce2
+class virtual ['a] located_iter      = ['a] Morbig.CSTVisitors.located_iter
+class virtual ['a] located_map       = ['a] Morbig.CSTVisitors.located_map
+class virtual ['a] located_reduce    = ['a] Morbig.CSTVisitors.located_reduce
+class virtual ['a] located_mapreduce = ['a] Morbig.CSTVisitors.located_mapreduce
+class virtual ['a] located_iter2     = ['a] Morbig.CSTVisitors.located_iter2
+class virtual ['a] located_map2      = ['a] Morbig.CSTVisitors.located_map2
+class virtual ['a] located_reduce2   = ['a] Morbig.CSTVisitors.located_reduce2
 
 let dummily_located value =
   { value ; position = Morbig.CSTHelpers.dummy_position }
 
+let copy_location : 'a 'b. 'a located -> 'b -> 'b located =
+  fun located value ->
+  { value ; position = located.position }
+
 let on_located f v =
   f v.value
diff --git a/src/safePrinter.ml b/src/safePrinter.ml
index 2ca778a..5efd322 100644
--- a/src/safePrinter.ml
+++ b/src/safePrinter.ml
@@ -144,7 +144,7 @@ and pp_command ppf (command : command) =
     match command with
 
     | Async command ->
-       pp_command ppf command
+       pp_command' ppf command
 
     | Seq (command1, command2) ->
        fpf ppf "%a;%a"
@@ -193,11 +193,11 @@ and pp_command ppf (command : command) =
     | For (variable, Some words, body) ->
        fpf ppf "for %a in %a;do %a;done"
          pp_name variable
-         pp_words words
+         pp_words' words
          pp_command' body
 
     | Case (word, items) ->
-       fpf ppf "case %a in" pp_word word;
+       fpf ppf "case %a in" pp_word' word;
        List.iter
          (fun item ->
            match item.Location.value with
@@ -238,7 +238,7 @@ and pp_command ppf (command : command) =
          pp_command' command
          descr
          pp_redirection_kind kind
-         pp_word file
+         pp_word' file
 
     | HereDocument (command, descr, content) ->
        (* if content.value.[String.length content.value - 1] <> '\n' then
