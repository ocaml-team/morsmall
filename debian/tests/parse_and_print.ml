let filename = Sys.argv.(1)
let ast = Morsmall.parse_file filename
let _ = Morsmall.pp_print_safe Format.std_formatter ast
